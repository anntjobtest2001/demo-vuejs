import Vue from "vue";
import App from "./App.vue";
import moment from "moment";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "jquery";
import "@popperjs/core";
// import { library } from "@fortawesome/fontawesome-svg-core";

// import { faHatWizard } from "@fortawesome/free-solid-svg-icons";

// import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
// library.add(faHatWizard);
import "bootstrap-icons/font/bootstrap-icons.css";
// import "lodash";
Vue.config.productionTip = false;
Vue.prototype.moment = moment;
new Vue({
  render: (h) => h(App),
}).$mount("#app");
